#!/usr/bin/env python

from __future__ import print_function

import sys, os

from xml.etree import ElementTree

# --------------------------------------------------------------------------

def convText (value) :
    if sys.version_info >= (3,) :
       if isinstance (value, bytearray) :
          value = value.decode ("utf-8")
    else :
       if value != None :
          value = value.encode ('utf-8')
    return value


def readItem (root, block) :
    name = root.tag
    value = root.text
    value = convText (value)
    #
    print ("      ", name, "=", value)
    block [name] = value

def readAttr (attr, block) :
    for name, val in attr.items():
       value = convText (val)
       #
       print ("      ", name, "=", value)
       block [name] = value

def readSequenceItem (root, sequence) :
    name = root.tag
    block = { }
    block ["type"] = name
    #
    print ("   Block", name)
    readAttr (root.attrib, block)
    for item in root :
        readItem (item, block)
    sequence.append (block)

def readDictionaryItem (root, dictionary) :
    name = root.tag
    block = { }
    block ["type"] = name
    #
    print ("   Block", name)
    readAttr (root.attrib, block)
    for item in root :
        readItem (item, block)
    key = "id"
    val = block [key]
    if val in dictionary :
       raise AssertionError ("duplicated key: " + val + " in " + name)
    dictionary [val] = block

def readTable (root, data) :
    name = root.tag
    if name in [ "courses_groups", "courses_lecturers", "comments_courses_groups", "courseSeries_comments" ] :
       sequence = [ ]
       data [name] = sequence
       # print ("Sequence", name)
       for item in root :
           readSequenceItem (item, sequence)
    else :
       dictionary = {  }
       data [name] = dictionary
       # print ("Dictionary", name)
       for item in root :
           readDictionaryItem (item, dictionary)


def readFile (fileName, data) :
    tree = ElementTree.parse (fileName)
    root = tree.getroot()
    root = root[0]
    # print (root.tag) # WhiteBook
    for item in root :
        readTable (item, data)

# --------------------------------------------------------------------------

def from_ak (type, name, year) :
    result = False
    year = int (year) # convert text to number

    if type == "BS" :
       if name in ["MM", "MF", "MINF", "AMSM", "APIN", "JCF", "IPL", "FIMAT", "LTF", "PF", "FPTF", "JCH", "DC"] :
          if year <= 1 :
             result = True
       if name in ["RT"] :
          if year <= 2 :
             result = True

    if type == "NMS" :
       if name in ["MI", "MINF", "MF", "AMSM",  "AFI", "JI", "JCF", "LFT", "FOT", "PF", "IPL", "FIMAT", "FPTF", "JCH", "DC" ] :
          if year <= 1 :
             result = True
       if name in ["RF"] :
          if year <= 2 :
             result = True

    return result

# --------------------------------------------------------------------------

def from_bk (type, name, year) :
    result = False
    year = int (year) # convert text to number

    if type == "BS" :
       if name in ["ASID", "ASIP", "JI", "DAIZ", "LPT", "FYT"] :
          result = True
       if name in ["MM", "MF", "AMSM", "MINF", "IF", "APIN", "EJCF", "IPL", "DM", "FTTF", "FE", "JCH" ] :
          if year >= 2 :
             result = True
       if name in ["RT"] :
          if year >= 3 :
             result = True

    if type == "NMS" :
       if name in ["ASI"] :
          result = True
       if name in ["MI", "MF", "AMSM", "MINF", "IF", "JI", "DAIZ", "EJCF", "IPL", "DM", "FTTF", "LTE", "ON", "JCH" ] :
          if year >= 2 :
             result = True

    return result

# --------------------------------------------------------------------------

def select (data, select_winter, select_ak) :
    answer = [ ]

    courses = data ["courses"]
    departments = data ["departments"]
    courses_groups = data ["courses_groups"]
    groups = data ["groups"]
    specializations = data ["specializations"]
    fieldsOfStudy = data ["fieldsOfStudy"]
    typesOfStudy = data ["typesOfStudy"]
    courses_lecturers = data ["courses_lecturers"]
    lecturers = data ["lecturers"]

    for course_group in courses_groups :
       group = groups [course_group ["idGroup"]]
       spec = specializations [group ["idSpecialization"]]
       fieldOfStudy = fieldsOfStudy [spec ["idFieldOfStudy"]]
       typeOfStudy = typesOfStudy [fieldOfStudy ["idTypeOfStudy"]]
       course = courses [course_group ["idCourse"]]

       if select_winter :
          valid = (course ["winter"] == "1")
          tag = course ["winterTag"]
          hours = course ["winterHours"]
       else :
          valid = (course ["summer"] == "1")
          tag = course ["summerTag"]
          hours = course ["summerHours"]

       if tag == None :
          tag = ""

       type = typeOfStudy ["acronym"] # BS, NMS
       year = group ["yearOfStudy"]
       name = spec ["acronym"]

       if valid :
          if select_ak :
             valid = from_ak (type, name, year)
             src = "ak"
          else :
             valid = not from_ak (type, name, year)
             src = "bk"
             if not from_bk (type, name, year) :
                src = "ck"

       if valid :
          result = { }
          answer.append (result)

          department = departments [course ["idDepartment"]]

          people = [ ]
          id = course ["id"]
          for item in courses_lecturers :
              if item ["idCourse"] == id :
                 lecturer = lecturers [item ["idLecturer"]]
                 people.append (lecturer["surname"])
          people.sort ()
          people_min = people [0]
          if len (people) > 1 :
             people_max = people [-1]
          else :
             people_max = ""

          result ["code"] = department["code"] + course ["acronym"] + tag
          result ["hours"] = hours
          result ["spec_id"] = name
          result ["type"] = type
          result ["year"] = year
          result ["mandatory"] = course_group ["mandatory"]
          result ["lecturers"] = ", ".join (people)
          result ["lecturer_min"] = people_min
          result ["lecturer_max"] = people_max

          result ["from"] = src

          result ["field_long_name"] = fieldOfStudy ["name"]
          result ["spec_long_name"] = spec ["name"]

    return answer

# --------------------------------------------------------------------------

def print_specializations (title, data) :
    specializations = data ["specializations"]
    fieldsOfStudy = data ["fieldsOfStudy"]
    typesOfStudy = data ["typesOfStudy"]

    result = [ ]
    for spec_inx in specializations :
        spec = specializations [spec_inx]
        fieldOfStudy = fieldsOfStudy [spec ["idFieldOfStudy"]]
        typeOfStudy = typesOfStudy [fieldOfStudy ["idTypeOfStudy"]]
        type = typeOfStudy ["acronym"] # BS, NMS
        name = spec ["acronym"]
        key = type + "," + name + "," +  fieldOfStudy ["name"] + "," + spec ["name"]
        if key not in result :
           result.append (key)

    result.sort ()
    print (title)
    for item in result :
        print ("   ", item)
    print ()

# --------------------------------------------------------------------------

def check_item (item, ak_items, bk_items, ck_items) :
    key = item ["type"] + "," + item ["year"] + "," + item ["spec_id"] + "," + item ["field_long_name"] + "," + item ["spec_long_name"]
    if item ["from"] == "ak" :
       if key not in ak_items :
          ak_items.append (key)
    if item ["from"] == "bk" :
       if key not in bk_items :
          bk_items.append (key)
    if item ["from"] == "ck" :
       if key not in ck_items :
          ck_items.append (key)

def print_items (fileName, ak_items, bk_items, ck_items) :
    print (fileName)

    print ("  from ak")
    ak_items.sort ()
    for txt in ak_items :
        print ("      ", txt)

    print ("  from bk")
    bk_items.sort ()
    for txt in bk_items :
        print ("      ", txt)

    print ("  strange from bk")
    ck_items.sort ()
    for txt in ck_items :
        print ("      ", txt)

    print ()

# --------------------------------------------------------------------------

def print_line (f, *values) :
    txt = ""
    inx = 0
    for val in values:
        if inx != 0 :
           txt = txt + ";"
        txt = txt + val
        inx = inx + 1
    print (txt, file=f)

def print_data (fileName, answer) :
    f = open (fileName, "w")
    ak_items = [ ]
    bk_items = [ ]
    ck_items = [ ]

    for item in answer :
       key = ( item ["code"].ljust (10) +
               item ["hours"].ljust (10) +
               item ["spec_id"].ljust (10) +
               item ["type"].ljust (10) +
               item ["year"].ljust (10) )
       item ["key"] = key

    answer.sort (key=lambda item: item ["key"])

    for item in answer :
        if item ["mandatory"] == "1" :
           mandatory = True
        else :
           mandatory = False

        if item ["from"] != "ck" :
           print_line (f,
                       item ["code"],
                       item ["hours"],
                       item ["spec_id"],
                       item ["type"],
                       item ["year"],
                       str (mandatory),
                       item ["lecturer_min"],
                       item ["lecturer_max"],
                       item ["from"] );

        check_item (item, ak_items, bk_items, ck_items)
    f.close ()
    print_items (fileName, ak_items, bk_items, ck_items)

# --------------------------------------------------------------------------

ak_data = { }
readFile ("AK_2020-8-15_11h17m.xml", ak_data)

bk_data = { }
readFile ("BK_2020-8-15_11h19m.xml", bk_data)

ak_answer = select (ak_data, select_winter=True, select_ak=True)
bk_answer = select (bk_data, select_winter=True, select_ak=False)
print_data ("zima-2020-2021.csv", ak_answer + bk_answer)

ak_answer = select (ak_data, select_winter=False, select_ak=True)
bk_answer = select (bk_data, select_winter=False, select_ak=False)
print_data ("leto-2020-2021.csv", ak_answer + bk_answer)

print_specializations ("all ak specializations", ak_data)
print_specializations ("all bk specializations", bk_data)

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
