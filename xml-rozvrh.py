#!/usr/bin/env python

from __future__ import print_function

import sys, os

from xml.etree import ElementTree

# --------------------------------------------------------------------------

def readItem (root, block) :
    name = root.tag
    value = root.text
    if value != None :
       value = value.encode('utf-8')
    # print ("      ", name, "=", value)
    block [name] = value

def readAttr (attr, block) :
    for name, value in attr.items():
       # print ("      ", name, "=", value)
       block [name] = value

def readSequenceItem (root, sequence) :
    name = root.tag
    block = { }
    block ["type"] = name
    # print ("   Block", name)
    readAttr (root.attrib, block)
    for item in root :
        readItem (item, block)
    sequence.append (block)

def readDictionaryItem (root, dictionary) :
    name = root.tag
    block = { }
    block ["type"] = name
    # print ("   Block", name)
    readAttr (root.attrib, block)
    for item in root :
        readItem (item, block)
    key = "id"
    val = block [key]
    if val in dictionary :
       raise AssertionError ("duplicated key: " + val + " in " + name)
    dictionary [val] = block

def readTable (root, data) :
    name = root.tag
    if name in [ "courses_groups", "courses_lecturers", "comments_courses_groups", "courseSeries_comments" ] :
       sequence = [ ]
       data [name] = sequence
       # print ("Sequence", name)
       for item in root :
           readSequenceItem (item, sequence)
    else :
       dictionary = {  }
       data [name] = dictionary
       # print ("Dictionary", name)
       for item in root :
           readDictionaryItem (item, dictionary)


def readFile (fileName, data) :
    tree = ElementTree.parse (fileName)
    root = tree.getroot()
    root = root[0]
    # print (root.tag) # WhiteBook
    for item in root :
        readTable (item, data)

# --------------------------------------------------------------------------

def select (data, select_winter) :
    answer = [ ]

    courses = data ["courses"]
    departments = data ["departments"]
    courses_groups = data ["courses_groups"]
    groups = data ["groups"]
    specializations = data ["specializations"]
    fieldsOfStudy = data ["fieldsOfStudy"]
    typesOfStudy = data ["typesOfStudy"]
    courses_lecturers = data ["courses_lecturers"]
    lecturers = data ["lecturers"]

    for course_group in courses_groups :
       group = groups [course_group ["idGroup"]]
       spec = specializations [group ["idSpecialization"]]
       fieldOfStudy = fieldsOfStudy [spec ["idFieldOfStudy"]]
       typeOfStudy = typesOfStudy [fieldOfStudy ["idTypeOfStudy"]]
       course = courses [course_group ["idCourse"]]

       if select_winter :
          valid = (course ["winter"] == "1")
          tag = course ["winterTag"]
          hours = course ["winterHours"]
       else :
          valid = (course ["summer"] == "1")
          tag = course ["summerTag"]
          hours = course ["summerHours"]

       if tag == None :
          tag = ""

       if valid :
          result = { }
          answer.append (result)

          department = departments [course ["idDepartment"]]

          people = [ ]
          id = course ["id"]
          for item in courses_lecturers :
              if item ["idCourse"] == id :
                 lecturer = lecturers [item ["idLecturer"]]
                 people.append (lecturer["surname"])
          people.sort ()

          result ["code"] = department["code"] + course ["acronym"] + tag
          result ["hours"] = hours
          result ["spec"] = spec ["acronym"]
          result ["type"] = typeOfStudy ["acronym"]
          result ["year"] = group ["yearOfStudy"]
          result ["mandatory"] = course_group ["mandatory"]
          result ["lecturers"] = ", ".join (people)
          result ["lecturer_min"] = people [0]
          result ["lecturer_max"] = people [-1]

    return answer

# --------------------------------------------------------------------------

def print_line (*values) :
    txt = ""
    inx = 0
    for val in values:
        if inx != 0 :
           txt = txt + ";"
        txt = txt + val
        inx = inx + 1
    print (txt)

def print_data (answer) :
    for item in answer :
       key = ( item ["code"].ljust (10) +
               item ["hours"].ljust (10) +
               item ["spec"].ljust (10) +
               item ["type"].ljust (10) +
               item ["year"].ljust (10) )
       item ["key"] = key

    answer.sort (key=lambda item: item ["key"])

    for item in answer :
        if item ["mandatory"] == "1" :
           mandatory = True
        else :
           mandatory = False

        print_line (item ["code"],
                    item ["hours"],
                    item ["spec"],
                    item ["type"],
                    item ["year"],
                    str (mandatory),
                    item ["lecturer_min"],
                    item ["lecturer_max"]);

# --------------------------------------------------------------------------

data = { }
readFile ("AK_2019-8-2_14h47m.xml", data)

answer = select (data, select_winter=True)
print_data (answer)

answer = select (data, select_winter=False)
# print_data (answer)


# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
