#!/usr/bin/env python

from __future__ import print_function

import sys, os

from xml.etree import ElementTree
import optparse

debug = False # True ... print xml data

# --------------------------------------------------------------------------

def convText (value) :
    if sys.version_info >= (3,) :
       if isinstance (value, bytearray) :
          value = value.decode ("utf-8")
    else :
       if value != None :
          value = value.encode ('utf-8')
    return value


def readItem (root, block) :
    name = root.tag
    value = root.text
    value = convText (value)
    if debug : print ("      ", name, "=", value)
    block [name] = value

def readAttr (attr, block) :
    for name, val in attr.items():
       value = convText (val)
       if debug : print ("      ", name, "=", value)
       block [name] = value

def readSequenceItem (root, sequence) :
    name = root.tag
    block = { }
    block ["type"] = name
    if debug : print ("   Block", name)
    readAttr (root.attrib, block)
    for item in root :
        readItem (item, block)
    sequence.append (block)

def readDictionaryItem (root, dictionary) :
    name = root.tag
    block = { }
    block ["type"] = name
    if debug : print ("   Block", name)
    readAttr (root.attrib, block)
    for item in root :
        readItem (item, block)
    key = "id"
    val = block [key]
    if val in dictionary :
       raise AssertionError ("duplicated key: " + val + " in " + name)
    dictionary [val] = block

def readTable (root, data) :
    name = root.tag
    if name in [ "courses_groups", "courses_lecturers", "comments_courses_groups", "courseSeries_comments" ] :
       sequence = [ ]
       data [name] = sequence
       if debug : print ("Sequence", name)
       for item in root :
           readSequenceItem (item, sequence)
    else :
       dictionary = {  }
       data [name] = dictionary
       if debug : print ("Dictionary", name)
       for item in root :
           readDictionaryItem (item, dictionary)


def readFile (fileName, data) :
    tree = ElementTree.parse (fileName)
    root = tree.getroot()
    root = root[0]
    if debug : print (root.tag) # WhiteBook
    for item in root :
        readTable (item, data)

# --------------------------------------------------------------------------

def from_ak (type, name, year) :
    result = False
    year = int (year) # convert text to number

    if type == "BS" :
       if year == 1 :
         if name in ["MM", "MINF", "MF", "AMSM", "APIN",
                     "APIVP", "APIVD",
                     "JR", "APFIZ", "RZP", "RZP_D",
                     "JCF",
                     "IPL", "FIMAT", "LTF", "PF", "FPTF",
                     "RT",
                     "JCH",
                     "DC",
                     "QT",
                     "AAA"] :
             result = True
       if year == 2 :
         if name in ["MM", "MINF", "MF", "AMSM", "APIN",
                     "JCF",
                     "IPL", "FIMAT", "LTF", "PF", "FPTF",
                     "RT",
                     "JCH",
                     "DC",
                     "QT",
                     "AAA"] :
             result = True
       if year == 3 :
         if name in [ "RT" ] :
             result = True

    if type == "NMS" :
       if year == 1 :
          if name in ["MI", "MINF", "MF", "AMSM", "APIV",
                      "AFI", "JI",
                      "JCF", "IPL", "FIMAT",
                      "LFT", "FOT", "PF",
                      "FPTF", "RF", "JCH", "DC", "QT", "AAA" ] :
             result = True
       if year == 2 :
          if name in ["MI", "MINF", "MF", "AMSM", # NO "APIV",
                      "AFI", "JI",
                      "JCF", "IPL", "FIMAT",
                      "LFT", "FOT", "PF",
                      "FPTF", "RF", "JCH", "DC", "QT", "AAA" ] :
             result = True

    return result

# --------------------------------------------------------------------------

def from_bk (type, name, year) :
    result = False
    year = int (year) # convert text to number

    if type == "BS" :
       if year == 2 :
          if name in ["ASIP", "ASID", "JI", "DAIZ", "LPT", "FYT"] :
             result = True

       if year == 3 :
          if name in ["MM", "MF", "AMSM", "MINF",
                      "IF", "ASIP", "ASID", "APIN",
                      "JI", "DAIZ", "EJCF", "IPL", "DM",
                      "FTTF", "FE", "LPT", "FYT", "JCH" ] :
             result = True

    if type == "NMS" :
       if year == 2 :
          if name in ["ASI"] :
             result = True

    return result

# --------------------------------------------------------------------------

def select (data, select_winter, select_ak) :
    answer = [ ]

    courses = data ["courses"]
    departments = data ["departments"]
    courses_groups = data ["courses_groups"]
    groups = data ["groups"]
    specializations = data ["specializations"]
    fieldsOfStudy = data ["fieldsOfStudy"]
    typesOfStudy = data ["typesOfStudy"]
    courses_lecturers = data ["courses_lecturers"]
    lecturers = data ["lecturers"]

    for course_group in courses_groups :
       group = groups [course_group ["idGroup"]]
       spec = specializations [group ["idSpecialization"]]
       fieldOfStudy = fieldsOfStudy [spec ["idFieldOfStudy"]]
       typeOfStudy = typesOfStudy [fieldOfStudy ["idTypeOfStudy"]]
       course = courses [course_group ["idCourse"]]

       if select_winter :
          valid = (course ["winter"] == "1")
          tag = course ["winterTag"]
          hours = course ["winterHours"]
       else :
          valid = (course ["summer"] == "1")
          tag = course ["summerTag"]
          hours = course ["summerHours"]

       if tag == None :
          tag = ""

       type = typeOfStudy ["acronym"] # BS, NMS
       year = group ["yearOfStudy"]
       name = spec ["acronym"]

       if valid :
          if select_ak :
             if from_ak (type, name, year) :
                src = "ak"
             else :
                src = "ak_unused"
          else :
             valid = not from_ak (type, name, year)
             if from_bk (type, name, year) :
                src = "bk"
             else :
                src = "bk_unused"

       if valid :
          result = { }
          answer.append (result)

          department = departments [course ["idDepartment"]]

          people = [ ]
          id = course ["id"]
          for item in courses_lecturers :
              if item ["idCourse"] == id :
                 lecturer = lecturers [item ["idLecturer"]]
                 people.append (lecturer["surname"])
          people.sort ()
          if len (people) >= 1 :
             people_min = people [0]
          else :
             people_min = ""
          if len (people) > 1 :
             people_max = people [-1]
          else :
             people_max = ""

          result ["code"] = department["code"] + course ["acronym"] + tag
          result ["hours"] = hours
          result ["spec_id"] = name
          result ["type"] = type
          result ["year"] = year
          result ["mandatory"] = course_group ["mandatory"]
          result ["lecturers"] = ", ".join (people)
          result ["lecturer_min"] = people_min
          result ["lecturer_max"] = people_max

          result ["from"] = src

          result ["field_long_name"] = fieldOfStudy ["name"]
          result ["spec_long_name"] = spec ["name"]

          result ["name"] = course ["name"]
          result ["nameEN"] = course ["nameEN"]

    return answer

# --------------------------------------------------------------------------

def print_specializations (title, data) :
    specializations = data ["specializations"]
    fieldsOfStudy = data ["fieldsOfStudy"]
    typesOfStudy = data ["typesOfStudy"]

    result = [ ]
    for spec_inx in specializations :
        spec = specializations [spec_inx]
        fieldOfStudy = fieldsOfStudy [spec ["idFieldOfStudy"]]
        typeOfStudy = typesOfStudy [fieldOfStudy ["idTypeOfStudy"]]
        type = typeOfStudy ["acronym"] # BS, NMS
        name = spec ["acronym"]
        key = type + "," + name + "," +  fieldOfStudy ["name"] + "," + spec ["name"]
        if key not in result :
           result.append (key)

    result.sort ()
    print (title)
    for item in result :
        print ("   ", item)
    print ()

# --------------------------------------------------------------------------

def check_item (item, ak_items, bk_items, ak_unused, bk_unused) :
    key = item ["type"] + "," + item ["year"] + "," + item ["spec_id"] + "," + item ["field_long_name"] + "," + item ["spec_long_name"]
    if item ["from"] == "ak" :
       if key not in ak_items :
          ak_items.append (key)
    if item ["from"] == "bk" :
       if key not in bk_items :
          bk_items.append (key)
    if item ["from"] == "ak_unused" :
       if key not in ak_unused :
          ak_unused.append (key)
    if item ["from"] == "bk_unused" :
       if key not in bk_unused :
          bk_unused.append (key)

def print_items (fileName, ak_items, bk_items, ak_unused, bk_unused) :
    print (fileName)

    print ("  from ak")
    ak_items.sort ()
    for txt in ak_items :
        print ("      ", txt)

    print ("  from bk")
    bk_items.sort ()
    for txt in bk_items :
        print ("      ", txt)

    print ("  unused from ak")
    ak_unused.sort ()
    for txt in ak_unused :
        print ("      ", txt)

    print ("  unused from bk")
    bk_unused.sort ()
    for txt in bk_unused :
        print ("      ", txt)

    print ()

# --------------------------------------------------------------------------

def print_codes (codeFileName, all_items) :
    code_dict = { }
    code_list = [ ]
    for item in all_items :
        code = item ["code"]
        name = item ["name"]
        if code not in code_dict :
           old_name = name
           name = ""
           for c in old_name :
              if c >= ' ' :
                 name = name + c
              else :
                 name = name + ' '
           code_dict [code] = name
           code_list.append (code)

    code_list.sort ()

    f = open (codeFileName, "w")
    for code in code_list :
        name = code_dict [code]
        print (code + ";" + name, file=f)
    f.close ()

# --------------------------------------------------------------------------

def print_line (f, *values) :
    txt = ""
    inx = 0
    for val in values:
        if inx != 0 :
           txt = txt + ";"
        txt = txt + val
        inx = inx + 1
    print (txt, file=f)

def print_data (fileName, answer, infoFileName) :
    f = open (fileName, "w")
    ak_items = [ ]
    bk_items = [ ]
    ak_unused = [ ]
    bk_unused = [ ]
    all_items = [ ]

    for item in answer :
       key = ( item ["code"].ljust (10) +
               item ["hours"].ljust (10) +
               item ["spec_id"].ljust (10) +
               item ["type"].ljust (10) +
               item ["year"].ljust (10) )
       item ["key"] = key

    answer.sort (key=lambda item: item ["key"])

    for item in answer :
        if item ["mandatory"] == "1" :
           mandatory = True
        else :
           mandatory = False

        if item ["from"] == "ak" or item ["from"] == "bk" :
           all_items.append (item)
           print_line (f,
                       item ["code"],
                       item ["hours"],
                       item ["spec_id"],
                       item ["type"],
                       item ["year"],
                       str (mandatory),
                       item ["lecturer_min"],
                       item ["lecturer_max"],
                       item ["from"] );

        check_item (item, ak_items, bk_items, ak_unused, bk_unused)

    f.close ()

    print_codes ("predmety-" + fileName, all_items)

    sys.stdout = open (infoFileName, 'w')
    print_items (fileName, ak_items, bk_items, ak_unused, bk_unused)
    print_specializations ("all ak specializations", ak_data)
    print_specializations ("all bk specializations", bk_data)

# --------------------------------------------------------------------------

options = optparse.OptionParser ()
options.add_option ("-l", "--leto", dest="leto", action="store_true", help="Letni semestr")
(opts, args) = options.parse_args ()

ak_data = { }
readFile ("new_AK_2021-8-2_11h52m.xml", ak_data)

bk_data = { }
readFile ("old_BK_2021-8-2_11h51m.xml", bk_data)

if not opts.leto :
   ak_answer = select (ak_data, select_winter=True, select_ak=True)
   bk_answer = select (bk_data, select_winter=True, select_ak=False)
   print_data ("zima-2021-2022.csv", ak_answer + bk_answer, "zima.txt")
else :
   ak_answer = select (ak_data, select_winter=False, select_ak=True)
   bk_answer = select (bk_data, select_winter=False, select_ak=False)
   print_data ("leto-2021-2022.csv", ak_answer + bk_answer, "leto.txt")

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
